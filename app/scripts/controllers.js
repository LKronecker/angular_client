'use strict';
angular.module('puffpuffApp')
.controller('SelectionController', ['$scope', 'flavorFactory','ngDialog', '$localStorage', function($scope, flavorFactory, ngDialog, $localStorage) {
    
    // Get data from the backend
    $scope.flavors = flavorFactory.getFlavors().query();
    $scope.positiveEff = flavorFactory.getPositive().query();
    $scope.negativeEff = flavorFactory.getNegative().query();
    $scope.meicalEff = flavorFactory.getMedical().query();
    
    // These arrays hold the user selection
    $scope.preferedFlavors = [];  
    $scope.preferedPositive = [];
    $scope.preferedNegative = [];
    $scope.preferedMedical = [];
    
    // These guys here hold a boolean value deending on user pref selection. They are used for UI selection/deselaction
    $scope.selectedFlavorsBools = [];
    $scope.selectedPositiveBools = [];
    $scope.selectedNegativeBools = [];
    $scope.selectedMedicalBools = [];
    
    $scope.setPreferedFlavor = function(pref, index) {
        if ($scope.preferedFlavors.indexOf(pref) < 0) { //element not in array
            $scope.preferedFlavors.push(pref);
            $scope.selectedFlavorsBools[index] = true;
        }
        else {
            $scope.preferedFlavors.splice($scope.preferedFlavors.indexOf(pref), 1 );
            $scope.selectedFlavorsBools[index] = false;
        }
    };
    
    $scope.setPreferedPositiveEff = function(pref, index) {
        if ($scope.preferedPositive.indexOf(pref) < 0) { //element not in array
            $scope.preferedPositive.push(pref);
            $scope.selectedPositiveBools[index] = true;
        }
        else {
            $scope.preferedPositive.splice($scope.preferedFlavors.indexOf(pref), 1 );
            $scope.selectedPositiveBools[index] = false;
        }
    };
    
    $scope.setPreferedNegativeEff = function(pref, index) {
        if ($scope.preferedNegative.indexOf(pref) < 0) { //element not in array
            $scope.preferedNegative.push(pref);
            $scope.selectedNegativeBools[index] = true;
        }
        else {
            $scope.preferedNegative.splice($scope.preferedFlavors.indexOf(pref), 1 );
            $scope.selectedNegativeBools[index] = false;
        }
    };
    
    $scope.setPreferedMedicalEff = function(pref, index) {
        if ($scope.preferedMedical.indexOf(pref) < 0) { //element not in array
            $scope.preferedMedical.push(pref);
            $scope.selectedMedicalBools[index] = true;
        }
        else {
            $scope.preferedMedical.splice($scope.preferedFlavors.indexOf(pref), 1 );
            $scope.selectedMedicalBools[index] = false;
        }
    };
    
    $scope.openConfirm = function () {
        $localStorage.storeObject('userpreferences', createUserPrefsDict());
        
        ngDialog.open({ template: 'views/confirm.html', 
                        scope: $scope, 
                        className: 'ngdialog-theme-default', 
                        controller:"ConfirmController", 
                        showClose: true,
                        closeByDocument: true,
                        closeByEscape: true,
                        appendTo: false});
    };
    
    function createUserPrefsDict() { 
        var userPrefs = {};
        userPrefs.flavors = $scope.preferedFlavors;
        userPrefs.positive = $scope.preferedPositive;
        userPrefs.negative = $scope.preferedNegative;
        userPrefs.medical = $scope.preferedMedical;
        
        return userPrefs;
    }

}])

.controller('HeaderController', ['$scope', '$state', '$rootScope', 'ngDialog', 'AuthFactory', function ($scope, $state, $rootScope, ngDialog, AuthFactory) {

    $scope.loggedIn = false;
    $scope.username = '';
    
    if(AuthFactory.isAuthenticated()) {
        $scope.loggedIn = true;
        $scope.username = AuthFactory.getUsername();
    }
    
    $scope.logOut = function() {
       AuthFactory.logout();
        $scope.loggedIn = false;
        $scope.username = '';
    };
    
    $rootScope.$on('login:Successful', function () {
        $scope.loggedIn = AuthFactory.isAuthenticated();
        $scope.username = AuthFactory.getUsername();
    });
        
    $rootScope.$on('registration:Successful', function () {
        $scope.loggedIn = AuthFactory.isAuthenticated();
        $scope.username = AuthFactory.getUsername();
    });
    
    $scope.stateis = function(curstate) {
       return $state.is(curstate);  
    };
    
    $scope.openLogin = function () {
        
        ngDialog.open({ template: 'views/login.html', 
                        scope: $scope, 
                        className: 'ngdialog-theme-default', 
                        controller:"LoginController", 
                        showClose: true,
                        closeByDocument: true,
                        closeByEscape: true,
                        appendTo: false});
    };
    
}])

.controller('ConfirmPageController', ['$scope','$localStorage','AuthFactory', function ($scope, $localStorage, AuthFactory) {
    
}])

.controller('ConfirmController', ['$scope', 'ngDialog','$localStorage','AuthFactory', function ($scope, ngDialog, $localStorage, AuthFactory) { 
    
    $scope.userPrefs = $localStorage.getObject('userpreferences','{}');
    console.log($scope.userPrefs);
    
//    $scope.getUserPref = function(prefString) {
//        return createStringFromList($scope.userPrefs.prefString);
//    };
//    
//    function createStringFromList(list) {
//        var initialString = "";
//        for(var item in list){
//            initialString = initialString + item;
//        }
//        console.log(initialString);
//        return initialString;
//    }
    
    $scope.submit = function() {
        console.log("Submit");
        ngDialog.close();
    };
            
    $scope.cancel = function () {
        console.log("Login");
    };
    
}])

.controller('LoginController', ['$scope', 'ngDialog', '$localStorage', 'AuthFactory', function ($scope, ngDialog, $localStorage, AuthFactory) {
    
    $scope.loginData = $localStorage.getObject('userinfo','{}');
    
    $scope.doLogin = function() {
        if($scope.rememberMe)
           $localStorage.storeObject('userinfo',$scope.loginData);

        AuthFactory.login($scope.loginData);

        ngDialog.close();

    };
            
//    $scope.openRegister = function () {
//        ngDialog.open({ template: 'views/register.html', scope: $scope, className: 'ngdialog-theme-default', controller:"RegisterController" });
//    };
    
}])

.controller('StrainController', ['$scope', 'strainFactory', function($scope, strainFactory) {
    
    $scope.message = "Loading ...";
    $scope.showMenu = true;
    $scope.showDetails = true;
    $scope.strains = strainFactory.getStrains();
    
    $scope.select = function(setTab) {
    $scope.tab = setTab;
    
    if (setTab === 2) {
      $scope.filtText = "sativa";
    } 
    else if (setTab === 3) {
      $scope.filtText = "indica";
    }
    else if (setTab === 4) {
      $scope.filtText = "hybrid";
    }
    else {
      $scope.filtText = "";
    }
  };
  
  $scope.isSelected = function (checkTab) {
    return ($scope.tab === checkTab);
  };
  
  $scope.toggleDetails = function() {
    $scope.showDetails = !$scope.showDetails;
      };

}])

.controller('StrainDetailController', ['$scope', '$stateParams', 'strainFactory', function($scope, $stateParams, strainFactory) {
            $scope.showDish = true;
            $scope.message="Loading ...";
            $scope.dish = strainFactory.getDishes().get({id:parseInt($stateParams.id,10)});
                    }])

;

