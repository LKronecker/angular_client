'use strict';
angular.module('puffpuffApp', ['ui.router','ngResource','ngDialog'])
.config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
                    // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller  : 'HeaderController'
                    },
                    'content': {
                        templateUrl : 'views/userselection.html',
                        controller  : 'SelectionController'
                    },
                    'footer': {
                        templateUrl : 'views/footer.html'
                    }
                }
            })

            // route for the menu page
            .state('app.strains', {
                url: 'strains',
                views: {
                    'content@': {
                        templateUrl : 'views/strainmenu.html',
                        controller  : 'StrainController'
                    }
                }
            })

            // route for the dishdetail page
            .state('app.straindetails', {
                url: 'strains/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/straindetail.html',
                        controller  : 'StrainDetailController'
                   }
                }
            })
        
        //route for the confim page
        .state('app.confirm', {
            url:'confirm',
            views: {
                    'content@': {
                        templateUrl : 'views/confirmpage.html',
                        controller  : 'ConfirmPageController'
                   }
                }
            })
        
          // route for the aboutus page
            .state('app.aboutus', {
                url:'aboutus',
                views: {
                    'content@': {
                        template: '<h1>To be Completed</h1>'
                   }
                }
            })
                    // route for the contactus page
            .state('app.contactus', {
              url:'contactus',
                views: {
                    'content@': {
                        templateUrl : 'views/contactus.html',
                        controller  : 'ContactController'
                     }
                }
            });
            $urlRouterProvider.otherwise('/');
    })

;