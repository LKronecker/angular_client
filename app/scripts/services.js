'use strict';

angular.module('puffpuffApp')
.constant("baseURL","https://localhost:3443/")

.service('flavorFactory', ['$resource', 'baseURL', function($resource,baseURL) {
 
    this.getFlavors = function(){
        var flavors = $resource(baseURL+"flavors/:id",null,  {'update':{method:'GET' }});
        return flavors;
                                };
    
    this.getPositive = function(){
        var positiveEff = $resource(baseURL+"positive/:id",null,  {'update':{method:'GET' }});
        return positiveEff;
                                };
    
    this.getNegative = function(){
        var negativeEff = $resource(baseURL+"negative/:id",null,  {'update':{method:'GET' }});
        return negativeEff;
                                };
    
    this.getMedical = function(){
        var medicalEff = $resource(baseURL+"medical/:id",null,  {'update':{method:'GET' }});
        return medicalEff;
                                };
    
        }])

.factory('$localStorage', ['$window', function ($window) {
    return {
        store: function (key, value) {
            $window.localStorage[key] = value;
        },
        get: function (key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        remove: function (key) {
            $window.localStorage.removeItem(key);
        },
        storeObject: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key, defaultValue) {
            return JSON.parse($window.localStorage[key] || defaultValue);
        }
    };
}])

.factory('AuthFactory', ['$resource', '$http', '$localStorage', '$rootScope', '$window', 'baseURL', 'ngDialog', function($resource, $http, $localStorage, $rootScope, $window, baseURL, ngDialog){
    
    var authFac = {};
    var TOKEN_KEY = 'Token';
    var isAuthenticated = false;
    var username = '';
    var authToken = undefined;
    

  function loadUserCredentials() {
    var credentials = $localStorage.getObject(TOKEN_KEY,'{}');
    if (credentials.username !== undefined) {
      useCredentials(credentials);
    }
  }
 
  function storeUserCredentials(credentials) {
    $localStorage.storeObject(TOKEN_KEY, credentials);
    useCredentials(credentials);
  }
 
  function useCredentials(credentials) {
    isAuthenticated = true;
    username = credentials.username;
    authToken = credentials.token;
 
    // Set the token as header for your requests!
    $http.defaults.headers.common['x-access-token'] = authToken;
  }
 
  function destroyUserCredentials() {
    authToken = undefined;
    username = '';
    isAuthenticated = false;
    $http.defaults.headers.common['x-access-token'] = authToken;
    $localStorage.remove(TOKEN_KEY);
  }
     
    authFac.login = function(loginData) {
        
        $resource(baseURL + "users/login")
        .save(loginData,
           function(response) {
              storeUserCredentials({username:loginData.username, token: response.token});
              $rootScope.$broadcast('login:Successful');
           },
           function(response){
              isAuthenticated = false;
            
              var message = '\
                <div class="ngdialog-message">\
                <div><h3>Login Unsuccessful</h3></div>' +
                  '<div><p>' +  response.data.err.message + '</p><p>' +
                    response.data.err.name + '</p></div>' +
                '<div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click=confirm("OK")>OK</button>\
                </div>';
            
                ngDialog.openConfirm({ template: message, plain: 'true'});
           }
        
        );

    };
    
    authFac.logout = function() {
        $resource(baseURL + "users/logout").get(function(response){
        });
        destroyUserCredentials();
    };
    
    authFac.register = function(registerData) {
        
        $resource(baseURL + "users/register")
        .save(registerData,
           function(response) {
              authFac.login({username:registerData.username, password:registerData.password});
            if (registerData.rememberMe) {
                $localStorage.storeObject('userinfo',
                    {username:registerData.username, password:registerData.password});
            }
           
              $rootScope.$broadcast('registration:Successful');
           },
           function(response){
            
              var message = '\
                <div class="ngdialog-message">\
                <div><h3>Registration Unsuccessful</h3></div>' +
                  '<div><p>' +  response.data.err.message + 
                  '</p><p>' + response.data.err.name + '</p></div>';

                ngDialog.openConfirm({ template: message, plain: 'true'});

           }
        
        );
    };
    
    authFac.isAuthenticated = function() {
        return isAuthenticated;
    };
    
    authFac.getUsername = function() {
        return username;  
    };

    loadUserCredentials();
    
    return authFac;
    
}])
          

    .service('strainFactory', function() {
    
    var strains=[
                        {
    "_id": "0",
    "category": "Hybrid",
    "description": "<div itemprop=\"description\">\n<p>Also known as <a href=\"http://www.leafly.com/hybrid/kosher-tangie\">Kosher Tangie</a>, 24k Gold is a 60% indica-dominant hybrid that combines the legendary LA strain <a href=\"http://www.leafly.com/indica/kosher-kush\">Kosher Kush</a> with champion sativa <a href=\"http://www.leafly.com/sativa/tangie\">Tangie</a> to create something quite unique. Growing tall in its vegetative cycle and very stretchy in flower, this one will need an experienced hand when grown indoors. Most <a href=\"http://www.leafly.com/news/cannabis-101/cannabis-genotypes-and-phenotypes-what-makes-a-strain-unique\">phenotypes</a> will exhibit a sweet orange aroma from the Tangie along with the dark coloration of the Kosher Kush, and will offer a strong citrus flavor when smoked or vaped. THC levels range from 18% to 24%; definitely not for novice users! </p>\n</div>",
    "fiveStarRating": 4.2,
    "name": "24k Gold",
    "videos": [],
    "positiveAttributes": [],
    "photos": [],
    "negativeAttributes": [],
    "medicalAttributes": [],
    "lineages": [
      "Kosher Kush",
      "Tangie"
    ],
    "flavors": [
      "Citrus",
      "Pine",
      "Pungent"
    ]
  },
  {
    "_id": "1",
    "category": "Hybrid",
    "description": "<div itemprop=\"description\">\n<p>13 Dawgs is a <a href=\"../explore/category-hybrid.html\">hybrid</a> of <a href=\"http://www.leafly.com/indica/g-13\">G13</a> and <a href=\"chemdawg.html\">Chemdawg</a> genetics bred by Canadian LP <a href=\"http://www.leafly.com/dispensary-info/delta-9-biotech\">Delta 9 BioTech</a>. The two potent strains mix to create a balance between <a href=\"../explore/category-indica.html\">indica</a> and <a href=\"../explore/category-sativa.html\">sativa</a> effects. 13 Dawgs has a <a href=\"../explore/flavors-sweet.html\">sweet</a> <a href=\"http://www.leafly.com/explore/flavors-earthy\">earthy</a> musk that brings a blend of <a href=\"http://www.leafly.com/explore/flavors-woody\">woody</a> <a href=\"../explore/flavors-citrus.html\">citrus</a> flavors. The effects of 13 Dawgs induce a <a href=\"../explore/tags-happy.html\">happy</a>, <a href=\"../explore/tags-relaxed.html\">relaxed</a> body buzz with a <a href=\"http://www.leafly.com/explore/tags-creative\">creative</a> and <a href=\"http://www.leafly.com/explore/tags-focused\">focused</a> mind that counters <a href=\"http://www.leafly.com/explore/symptoms-depression\">depression</a> and <a href=\"../explore/tags-hungry.html\">stimulates the appetite</a>.</p>\n</div>",
    "fiveStarRating": 4.4,
    "name": "13 Dawgs",
    "videos": [],
    "positiveAttributes": [],
    "photos": [
      "https://leafly.blob.core.windows.net/reviews/13-dawgs_100x100_5618.jpg"
    ],
    "negativeAttributes": [],
    "medicalAttributes": [],
    "lineages": [
      "G13",
      "Chemdawg"
    ],
    "flavors": [
      "Apricot",
      "Blue Cheese",
      "Citrus"
    ]
  },
  {
    "_id": "2",
    "category": "Hybrid",
    "description": "<div itemprop=\"description\">\n<p>Chem Crush is a <a href=\"http://leafly.com/hybrid\">hybrid</a> strain that took 2nd place in the 2014 Denver Cannabis Cup.</p>\n</div>",
    "fiveStarRating": 4.1,
    "name": "Chem Crush",
    "videos": [],
    "positiveAttributes": [],
    "photos": [
      "https://leafly.blob.core.windows.net/reviews/chem-crush_100x100_4aa1.jpg",
      "https://leafly.blob.core.windows.net/reviews/chem-crush_100x100_e151.jpg"
    ],
    "negativeAttributes": [],
    "medicalAttributes": [],
    "lineages": [
      "Orange Crush"
    ],
    "flavors": [
      "Earthy",
      "Pungent",
      "Spicy/Herbal"
    ]
  }
                        ];
    
    this.getStrains = function(){
                                        return strains;
                                    };
                    this.getStrain = function (index) {
                                        return strains[index];

                };
    
        })
;